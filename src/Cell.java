import java.awt.*;

class Cell extends Rectangle {

    static int size = 35;
    char col;
    int row;
    AnimationBeat animationBeat = AnimationBeat.getInstance();

    public Cell(char col, int row, int x, int y) {
        super(x, y, size, size);
        this.col = col;
        this.row = row;
    }

    void paint(Graphics g, Point mousePos) {
        if (contains(mousePos)) {
            g.setColor(Color.GRAY);
        } else if (animationBeat.inPhase() == 'a') {
            if (animationBeat.phaseCompletion() <= 50) {
                g.setColor(Color.BLACK);
            } else {
                g.setColor(Color.WHITE);
            }
        } else if (animationBeat.inPhase() == 'b') {
            if (animationBeat.phaseCompletion() <= 50) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.BLUE);
            }
        } else {
            if (animationBeat.phaseCompletion() <= 50) {
                g.setColor(Color.GREEN);
            } else {
                g.setColor(Color.YELLOW);
            }
        }
        g.fillRect(x, y, size, size);
        g.setColor(Color.BLACK);
        g.drawRect(x, y, size, size);
    }

    public boolean contains(Point p) {
        if (p != null) {
            return super.contains(p);
        } else {
            return false;
        }
    }

    public int leftOfComparison(Cell c) {
        return Character.compare(col, c.col);
    }

    public int aboveComparison(Cell c) {
        return Integer.compare(row, c.row);
    }

}