import java.util.*;

public class CompositeActor extends Actor {

    List<Actor> actors = new ArrayList<Actor>();
    
    public void setPoly() {}

    public CompositeActor(Actor a) {
        actors.add(a);
        this.redness = a.redness;
        this.moves = a.moves;
    }

    public void add(Actor other) {
        actors.add(other);
        this.redness = (this.redness + other.redness)/2;
        this.moves = Math.min(this.moves, other.moves);
    }

}