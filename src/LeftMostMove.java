import java.util.*;

public class LeftMostMove implements MoveStrategy {

    @Override
    public Cell chooseNextLoc(List<Cell> possibleLocs) {
        Cell cur = possibleLocs.get(0);
        for(Cell c: possibleLocs) {
            if (c.leftOfComparison(cur) < 0) {
                cur = c;
            }
        }
        return cur;
    }

    @Override
    public String toString() {
        return "left-most movement strategy";
    }

}