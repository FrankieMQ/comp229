import java.awt.*;

public class ImpedimentAdapter extends Actor {

    Impediment<Cell> delegate;
    
    public ImpedimentAdapter(Impediment<Cell> delegate) {
        this.delegate = delegate;
        this.turns = 0;
        this.moves = 0;
    }

    @Override
    public void setPoly() {
        delegate.setPoly();
    }

    @Override
    public boolean isTeamRed() {
        return false;
    }

    @Override
    public boolean isTeamBlue() {
        return false;
    }

    @Override
    public void setLocation(Cell location) {
        delegate.moveAtOwnSpeed(location);
    }

    @Override
    public void paint(Graphics g) {
        delegate.paint(g);
    }

    @Override
    public Cell getLocation() {
        return delegate.getLocation();
    }

    @Override
    public float getRedness() {
        return 0.0f;
    }

    @Override
    public String getStrat() {
        return "do what I want";
    }
    
}