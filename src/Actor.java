import java.awt.*;
import java.util.ArrayList;

public abstract class Actor {
    
    Color colour;
    Cell loc;
    ArrayList<Polygon> display;
    MoveStrategy strat;
    AnimationBeat animationBeat = AnimationBeat.getInstance();
    float redness;
    int turns;
    int moves;
    
    public void paint(Graphics g) {
        for (Polygon p: display) {
            colour = new Color(redness, 0f, 1f-redness);
            if (animationBeat.inPhase() == 'a') {
                if (animationBeat.phaseCompletion() <= 50) {
                    colour = colour.brighter();
                } else {
                    colour = colour.darker();
                }
            } else if (animationBeat.inPhase() == 'b') {
                if (animationBeat.phaseCompletion() <= 50) {
                    colour = Color.BLACK;
                } else {
                    colour = Color.WHITE;
                }
            } else {
                if (animationBeat.phaseCompletion() <= 50) {
                    colour = Color.CYAN;
                } else {
                    colour = Color.MAGENTA;
                }
            }
            g.setColor(colour);
            g.fillPolygon(p);
            g.setColor(Color.GRAY);
            g.drawPolygon(p);
        }
    }

    public abstract void setPoly();

    public boolean isTeamRed() {
        return redness >= 0.5;
    }

    public boolean isTeamBlue() {
        return redness < 0.5;
    }

    public void setLocation(Cell loc) {
        this.loc = loc;
        if (this.loc.row % 2 == 0) {
            this.strat = new RandomMove();
        } else {
            this.strat = new LeftMostMove();
        }
        setPoly();
    }

    public void eat(Actor other) {
        this.redness = (this.redness + other.redness)/2;
        this.moves = Math.min(this.moves, other.moves);
    }

    public Cell getLocation() {
        return loc;
    }

    public float getRedness() {
        return redness;
    }

    public String getStrat() {
        return strat.toString();
    }
    
}